#include <iostream>
#include <vector>
#include <time.h>
#include "SurpriseTable.h"
#include "Rare.h"
#include "Potion.h"
#include "Bomb.h"
#include "Crystal.h"
#include "Unit.h"
#include "Utils.h"

using namespace std;

const int COST_OF_GACHA = 5, WIN_CONDITION_POINTS = 100;

void PlayAstonishingInteractions(Unit & player, SurpriseTable & lootTable);

int main() {
	srand(time(NULL));

	SurpriseTable * lootTable = new SurpriseTable("LT 1");
	lootTable->AddCapsule(new Capsule(new Rare("SSR", 50), 1));
	lootTable->AddCapsule(new Capsule(new Rare("SR", 10), 9));
	lootTable->AddCapsule(new Capsule(new Rare("R", 1), 40));
	lootTable->AddCapsule(new Capsule(new Potion("Health Potion", 30), 15));
	lootTable->AddCapsule(new Capsule(new Bomb("Bomb", 25), 20));
	lootTable->AddCapsule(new Capsule(new Crystal("Crystal", 15), 15));
	//lootTable->AddItem(new Capsule(new Bomb("Slender Female", 9001), 1));
	//lootTable->AddItem(new Capsule(new Bomb("Enormous Maiden", 1), 9));
	lootTable->CalculateTable();
	//lootTable->Display();

	//Create Player, 100 HP, 100 Crystals
	Unit * player = new Unit("Player", 100, 100); 
	player->Display();

	system("pause");
	system("cls");

	PlayAstonishingInteractions(*player, *lootTable);

	delete lootTable;
	delete player;
	return 0;
}

void PlayAstonishingInteractions(Unit & player, SurpriseTable & lootTable)
{
	bool didPlayerWin = false;
	int drawsAmt = 0;
	while (player.GetHealth() > 0 && player.GetCrystals() >= COST_OF_GACHA
		&& !didPlayerWin) {

		drawsAmt++;
		player.ModCrystal(-COST_OF_GACHA);
		player.AddItemToInventory(lootTable.CreateItemRandom());
		player.GetIventory().back()->Activate();

		player.Display();
		cout << "Draws #: " << drawsAmt
			<< "\n=================="
			<< "\nUsed " << COST_OF_GACHA << " Crystals to draw"; PrintEllipses(3, 100);
		cout << " " << player.GetIventory().back()->GetName() << endl;
		cout << "\t"; player.GetIventory().back()->DisplayActivationContext();

		system("pause");

		if (player.GetRarityPoints() >= WIN_CONDITION_POINTS)
			didPlayerWin = true;

		system("cls");
	}

	player.Display();
	cout << "Draws #: " << drawsAmt << endl;

	cout << "==================" << endl;
	if (didPlayerWin) cout << "Congrats!" << endl;
	else cout << "Oops! you lost!" << endl;

	cout << "==================" << endl;
	cout << "Printing inventory in the order of retrieval." << endl;
	player.DisplayInventoryLog();

	cout << "==================" << endl;
	cout << "Printing itemized inventory." << endl;
	player.DisplayInventoryItemized();

	system("pause");
}
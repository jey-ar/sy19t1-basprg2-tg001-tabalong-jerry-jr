#pragma once
#include "Item.h"
class Capsule
{
public:
	Capsule(Item * theItem, float weight);
	~Capsule();

	Item * GetItem();

	const float & GetWeight(); 
	const float & GetTablePosition(); 
	const float & GetIndividualChance();

	void CalculateChanceAndPosition(float totalWeight, float startingPercent = 0); 

private:
	Item * m_TheItemInside = nullptr;
	float m_Weight = 0, m_TablePosition = 0, m_IndChance = 0;
};
#pragma once
#include "Item.h"
class Potion :
	public Item
{
public:
	Potion(std::string name, int healAmt, Unit * owner = NULL);
	~Potion();

	void Activate() override;
	void DisplayActivationContext() override;
	Item * CreateDuplicateSelf() override;

private:
	int m_HealAmount;
};
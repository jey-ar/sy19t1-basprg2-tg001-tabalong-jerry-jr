#include "Capsule.h"



Capsule::Capsule(Item * theItem, float weight)
{
	m_TheItemInside = theItem;
	m_Weight = weight;
}


Capsule::~Capsule()
{
	if (m_TheItemInside != nullptr)
		delete m_TheItemInside;
}

Item * Capsule::GetItem()
{
	return m_TheItemInside;
}

const float & Capsule::GetWeight()
{
	return m_Weight;
}

const float & Capsule::GetTablePosition()
{
	return m_TablePosition;
}

const float & Capsule::GetIndividualChance()
{
	return m_IndChance;
}

void Capsule::CalculateChanceAndPosition(float totalWeight, float startingPercent)
{
	if (m_Weight >= totalWeight || totalWeight == 0) return;
	m_IndChance = m_Weight / totalWeight;
	m_TablePosition = startingPercent + m_IndChance;
}

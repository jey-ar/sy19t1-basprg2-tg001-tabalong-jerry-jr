#pragma once
#include "Item.h"
class Crystal :
	public Item
{
public:
	Crystal(std::string name, int addAmt, Unit* owner = NULL);
	~Crystal();

	void Activate() override;
	void DisplayActivationContext() override;
	Item * CreateDuplicateSelf() override;

private:
	int m_CrystalsToAdd = 0;
};
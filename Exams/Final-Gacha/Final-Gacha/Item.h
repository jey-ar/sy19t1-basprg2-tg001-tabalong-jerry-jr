#pragma once
#include <string>
#include "Unit.h"
class Unit;
class Item
{
public:
	Item(std::string name = "", Unit* owner = NULL);
	~Item();

	const std::string & GetName();

	virtual void Activate() = 0;
	virtual void DisplayActivationContext() = 0; 
	virtual Item * CreateDuplicateSelf() = 0;

	void SetOwner(Unit * owner);
	const Unit * GetOwner();

protected:
	std::string m_Name = "";
	Unit * m_Owner = nullptr;
};
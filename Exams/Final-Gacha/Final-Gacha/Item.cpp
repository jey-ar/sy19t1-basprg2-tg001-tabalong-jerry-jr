#include "Item.h"
#include <assert.h>



Item::Item(std::string name, Unit * owner)
{
	m_Name = name;
	m_Owner = owner;
}

Item::~Item()
{
}

const std::string & Item::GetName()
{
	return m_Name;
}

void Item::SetOwner(Unit * owner)
{
	m_Owner = owner;
}

const Unit * Item::GetOwner()
{
	if (m_Owner == nullptr) return nullptr;
	return m_Owner;
}
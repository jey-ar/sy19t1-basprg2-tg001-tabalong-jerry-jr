#include "Potion.h"



Potion::Potion(std::string name, int healAmt, Unit * owner)
	: Item(name, owner)
{
	m_HealAmount = healAmt;
}


Potion::~Potion()
{
}

void Potion::Activate()
{
	m_Owner->SetHealth(m_Owner->GetHealth() + m_HealAmount);
}

void Potion::DisplayActivationContext()
{
	std::cout << m_Owner->GetName() << " is healed by " 
		<< m_HealAmount << " points." 
		<< std::endl;
}

Item * Potion::CreateDuplicateSelf()
{
	return new Potion(m_Name, m_HealAmount, m_Owner);
}

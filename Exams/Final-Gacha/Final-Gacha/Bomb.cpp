#include "Bomb.h"



Bomb::Bomb(std::string name, int dmgAmt, Unit * owner)
	: Item(name, owner)
{
	m_DamageAmount = dmgAmt;
}

Bomb::~Bomb()
{
}

void Bomb::Activate()
{
	m_Owner->SetHealth(m_Owner->GetHealth() - m_DamageAmount);
}

void Bomb::DisplayActivationContext()
{
	std::cout << m_Owner->GetName() << " is hurt by " 
		<< m_DamageAmount << " points." 
		<< std::endl;
}

Item * Bomb::CreateDuplicateSelf()
{
	return new Bomb(m_Name, m_DamageAmount, m_Owner);
}

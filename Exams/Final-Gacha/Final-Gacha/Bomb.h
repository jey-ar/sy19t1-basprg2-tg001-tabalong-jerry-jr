#pragma once
#include "Item.h"
class Bomb :
	public Item
{
public:
	Bomb(std::string name, int dmgAmt, Unit* owner = NULL);
	~Bomb();

	void Activate() override;
	void DisplayActivationContext() override;
	Item * CreateDuplicateSelf() override;

private:
	int m_DamageAmount = 0;
};
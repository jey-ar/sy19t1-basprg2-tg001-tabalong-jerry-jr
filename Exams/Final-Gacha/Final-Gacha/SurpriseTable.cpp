#include <assert.h>
#include "SurpriseTable.h"



SurpriseTable::SurpriseTable(std::string name)
{
	m_Name = name;
}

SurpriseTable::~SurpriseTable()
{
	for (auto c : m_Capsules) 
		delete c;
	m_Capsules.clear();
	m_Capsules.shrink_to_fit();
}

const std::string & SurpriseTable::GetName()
{
	return m_Name;
}

void SurpriseTable::AddCapsule(Capsule * caps)
{
	assert(caps->GetWeight() > 0);
	m_Capsules.push_back(caps);
	m_TotalWeight += caps->GetWeight();
	isUpdated = false;
}

const std::vector<Capsule*> & SurpriseTable::GetCapsules()
{
	return m_Capsules;
}

void SurpriseTable::CalculateTable()
{
	if (m_TotalWeight == 0) return;

	float smallestPositiveFloat = 1 / FLT_MAX;
	float curPercent = 0.f;

	for (auto i : m_Capsules) {
		assert(curPercent < 1.f); //JIC

		i->CalculateChanceAndPosition(m_TotalWeight, curPercent);
		curPercent = i->GetTablePosition() + smallestPositiveFloat;
	}

	std::sort(m_Capsules.begin(), m_Capsules.end(), isPositionLess); //JIC

	isUpdated = true;
}

Item * SurpriseTable::CreateItemRandom()
{
	if (!isUpdated) CalculateTable(); //JIC
	float random = (float)rand() / RAND_MAX;

	for (auto i : m_Capsules) {
		if (i->GetTablePosition() >= random) {
			assert(i->GetItem()->GetOwner() == nullptr);
			return i->GetItem()->CreateDuplicateSelf();
		}
	}
	return nullptr;
}

void SurpriseTable::Display()
{
	std::cout << "LootTable: " << m_Name 
		<< "\tUnique Items: " << m_Capsules.size() 
		<< std::endl;

	for (auto c : m_Capsules) {
		std::cout << c->GetItem()->GetName().substr(0, 7)
			<< "\t" << c->GetWeight() 
			<< "\t" << c->GetIndividualChance() * 100 << "%"
			<< "\t" << c->GetTablePosition() 
			<< std::endl;
	}
}

bool SurpriseTable::isPositionLess(Capsule * a, Capsule * b)
{
	return (a->GetTablePosition() < b->GetTablePosition());
}
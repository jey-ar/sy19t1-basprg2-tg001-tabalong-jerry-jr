#pragma once
#include "Item.h"
class Rare :
	public Item
{
public:
	Rare(std::string name, int rarePoints, Unit * owner = NULL);
	~Rare();	

	void Activate() override;
	void DisplayActivationContext() override;
	Item * CreateDuplicateSelf() override;

	const int & GetRarePoints();

private:
	int m_RarePoints = 0;
};
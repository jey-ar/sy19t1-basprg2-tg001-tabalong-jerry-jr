#pragma once
#include <algorithm>
#include <vector>
#include "Item.h"
#include "Capsule.h"
class SurpriseTable
{
public:
	SurpriseTable(std::string name = "");
	~SurpriseTable();
	
	const std::string & GetName();

	void AddCapsule(Capsule * caps);
	const std::vector<Capsule*> & GetCapsules();
	void CalculateTable();
	Item * CreateItemRandom();

	void Display();
	
private:
	std::string m_Name;
	std::vector<Capsule*> m_Capsules;
	float m_TotalWeight = 0;
	bool isUpdated;

	static bool isPositionLess(Capsule * a, Capsule * b); //Used for sorting
};
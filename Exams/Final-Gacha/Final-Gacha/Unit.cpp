#include <string>
#include "Unit.h"



Unit::Unit(std::string name, int health, int crystals)
{
	m_Name = name;
	m_Health.first = health;
	m_Health.second = health;
	m_Crystals = crystals;
}

Unit::~Unit()
{
	for (auto i : m_Inventory) 
		delete i;
	m_Inventory.clear();
	m_Inventory.shrink_to_fit();
	for (auto im : m_ItemizedInventory)
		delete im.first;
	m_ItemizedInventory.clear();
	m_ItemizedInventory.shrink_to_fit();
}

const std::string & Unit::GetName()
{
	return m_Name;
}

const int & Unit::GetHealth(bool getMax)
{
	if (getMax) return m_Health.second;
	return m_Health.first;
}

void Unit::SetHealth(int value, bool setMax)
{
	if (value < 0) value = 0;
	if (setMax) { m_Health.second = value; return; }
	if (value > m_Health.second) value = m_Health.second;
	m_Health.first = value;
}

const int & Unit::GetCrystals()
{
	return m_Crystals;
}

void Unit::SetCrystals(int value)
{
	if (value < 0)  value = 0;
	m_Crystals = value;
}

void Unit::ModCrystal(int value)
{
	if (m_Crystals + value < 0) value = 0;
	m_Crystals += value;
}

const int & Unit::GetRarityPoints()
{
	return m_RarityPoints;
}

void Unit::SetRarityPoints(int value)
{
	if (value < 0) value = 0;
	m_RarityPoints = value;
}

void Unit::AddItemToInventory(Item * addItem)
{
	addItem->SetOwner(this);
	m_Inventory.push_back(addItem); 

	for (std::vector<std::pair<Item*, int>>::iterator it = m_ItemizedInventory.begin(); 
		it != m_ItemizedInventory.end(); it++) {

		if (it->first->GetName() == addItem->GetName()) {
			it->second++;
			return;
		}
	}
	m_ItemizedInventory.push_back(std::make_pair(addItem->CreateDuplicateSelf(), 1));
}

const std::vector<Item*> Unit::GetIventory()
{
	return m_Inventory;
}

const std::vector<std::pair<Item*, int>> Unit::GetItemizedInventory()
{
	return m_ItemizedInventory;
}

void Unit::Display()
{
	std::cout << "Name: " << m_Name 
		<< "\tHealth: " << m_Health.first << "/" << m_Health.second 
		<< "\nCrystals: " << m_Crystals
		<< "\nRarity Points: " << m_RarityPoints 
		<< std::endl;
}

void Unit::DisplayInventoryLog()
{
	for (auto i : m_Inventory) {
		std::cout << "\t" << i->GetName() << std::endl;
	}
}

void Unit::DisplayInventoryItemized()
{
	for (auto i : m_ItemizedInventory) {
		std::cout << "\t" << i.first->GetName() << " x" << i.second << std::endl;
	}
}

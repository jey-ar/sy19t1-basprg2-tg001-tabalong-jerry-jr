#pragma once
#include <string>
#include <vector>
#include <utility>
#include <iostream>
#include "Item.h"
class Item;
class Unit
{
public:
	Unit(std::string name, int health, int crystals);
	~Unit();
	
	const std::string & GetName();

	const int & GetHealth(bool getMax = false);
	void SetHealth(int value, bool setMax = false);

	const int & GetCrystals();
	void SetCrystals(int value);
	void ModCrystal(int value);

	const int & GetRarityPoints();
	void SetRarityPoints(int value);

	void AddItemToInventory(Item* addItem);
	const std::vector<Item*> GetIventory();
	const std::vector<std::pair<Item*, int>> GetItemizedInventory();

	void Display();
	void DisplayInventoryLog();
	void DisplayInventoryItemized();

private:
	std::string m_Name;
	std::pair<int, int> m_Health; //First - Current, Second - Max
	int m_Crystals = 0, m_RarityPoints = 0;

	std::vector<Item*> m_Inventory;
	std::vector<std::pair<Item*, int>> m_ItemizedInventory;
};
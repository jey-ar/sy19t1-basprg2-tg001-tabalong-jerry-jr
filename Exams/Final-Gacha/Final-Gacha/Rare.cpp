#include "Rare.h"



Rare::Rare(std::string name, int rarePoints, Unit * owner)
	: Item(name, owner)
{
	m_RarePoints = rarePoints;
}


Rare::~Rare()
{
}

void Rare::Activate()
{
	m_Owner->SetRarityPoints(m_Owner->GetRarityPoints() + m_RarePoints);
}

void Rare::DisplayActivationContext()
{
	std::cout << m_Owner->GetName() << " gained " 
		<< m_RarePoints << " points."
		<< std::endl;
}

Item * Rare::CreateDuplicateSelf()
{
	return new Rare(m_Name, m_RarePoints, m_Owner);
}

const int & Rare::GetRarePoints()
{
	return m_RarePoints;
}

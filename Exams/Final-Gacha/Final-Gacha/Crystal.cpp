#include "Crystal.h"



Crystal::Crystal(std::string name, int addAmt, Unit * owner)
	: Item(name, owner)
{
	m_CrystalsToAdd = addAmt;
}

Crystal::~Crystal()
{
}

void Crystal::Activate()
{
	m_Owner->SetCrystals(m_Owner->GetCrystals() + m_CrystalsToAdd);
}

void Crystal::DisplayActivationContext()
{
	std::cout << m_Owner->GetName() << " gained " 
		<< m_CrystalsToAdd << " crystals." 
		<< std::endl;
}

Item * Crystal::CreateDuplicateSelf()
{
	return new Crystal(m_Name, m_CrystalsToAdd, m_Owner);
}

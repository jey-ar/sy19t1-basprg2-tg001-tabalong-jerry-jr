#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <windows.h>

using namespace std;

// Midterms-Emperor-Cards.cpp - using vectors

//Card names should equal to card type
const string CARD_NAMES[4] = { "Slave", "Civilian", "Emperor", "Ultra Instinct Slave" };
const enum CardType { SLAVE, CIVILIAN, EMPEROR, ULTRA_INSTINCT_SLAVE };
const enum Result {lose, draw, win}; //Use enum instead of ints

//Card containing a CardType
struct Card {
	CardType type;
};

//Contains name, vector of Card as deck and money
struct Actor {
	string name;
	vector<Card> deck;
	int money;
};

//Collection of rules
struct Rules {
	int initialLength; //must be > 0
	string unitType; //e.g. mm, inches, lightyears
	int payoutPerLength; //How many currencyType to pay per length wagered
	string currencyType; //e.g. dollars, yen, pesos
	int slaveMult; //Amount to multiply payoutPerLength when slave
	int maxRounds; //How many rounds to play
	bool playerIsEmperorFirst; //Player gets to have it first 
	int swapCount; //How many turns player gets to be Emperor/Slave
	int maxCards; //Amount of cards to give
	int victoryAmount; //How many for Good Ending
};

//Print some dots
void PrintElipses(int amount, int waitTime = 200);

//Pick an int between/on min and max, returns int
int PickBetween(int min, int max);

//Give out Cards based on amount also decide if they get an Emperor or a Slave
void DistributeCards(vector<Card> &deck, int amount, bool IsEmperor);

//Show all Cards on the deck with an index
void ShowDeck(vector<Card> &deck, bool withIndent = true);

//Compare two Cards together, return a Result based on the perspective of the owner of the firstCard.
Result CompareTwoCards(Card &firstCard, Card &secondCard);

//Play until some conditions based on the provided Rules trigger
void EmperorCardGame(Actor &enemy, Actor &player, Rules rules);

int main() {
	srand(time(NULL));

	//Initialization
	Actor enemy = { "Tonegawa" };
	Actor player = { "Kaiji" };
	player.money = 0;

	//30 mm
	//100,000 Yen per mm
	//x5 when Slave
	//12 rounds
	//Player is Emperor first
	//Swap every 3 turns
	//Maximum of 5 cards
	//20,000,000 million in hand for best ending
	Rules rules = {30, "mm", 100000, "Yen", 5, 12, true, 3, 5, 20000000};

	EmperorCardGame(enemy, player, rules);

	cout << "=====GAME END=====" << endl; 
	system("pause");
	return 0;
}

void PrintElipses(int amount, int waitTime) {
	for (int i = 0; i < amount; i++) {
		cout << ". ";
		Sleep(waitTime);
	}
	cout << endl;
	return;
}

int PickBetween(int min, int max) {
	int n = INT_MIN;
	while (n < min || n > max) {
		cin >> n;
		if (n < min || n > max) {
			cin.clear();
			cin.ignore(INT_MAX, '\n');
		}
	}
	return n;
}

void DistributeCards(vector<Card> &deck, int amount, bool IsEmperor) {
	//First Card to recieve is either an Emperor or a Slave, then rest are Civilians
	for (int i = amount; i > 0; i--) {
		if (i == amount) {
			IsEmperor ? deck.push_back({ EMPEROR }) : deck.push_back({ SLAVE });
		}
		else {
			deck.push_back({ CIVILIAN });
		}
	}
	return;
}

void ShowDeck(vector<Card> &deck, bool withIndent) {
	for (int i = 0; i < deck.size(); i++) {
		if (withIndent) cout << "\t";
		cout << "[" << (i + 1) << "] = " << (CARD_NAMES[deck[i].type]) << endl;
	}
	return;
}

Result CompareTwoCards(Card &firstCard, Card &secondCard) {
	//Both Civilians is a draw
	if (firstCard.type == secondCard.type == CIVILIAN) return draw;
	//Slave against Emperor turns the slave into an ultra instint slave
	if (firstCard.type == SLAVE && secondCard.type == EMPEROR) firstCard.type = ULTRA_INSTINCT_SLAVE;
	if (firstCard.type == EMPEROR && secondCard.type == SLAVE) secondCard.type = ULTRA_INSTINCT_SLAVE;
	//Emperor against Civs and Civs against Slave is a win, reverse is lose
	if (firstCard.type > secondCard.type) return win; else return lose;
}

void EmperorCardGame(Actor &enemy, Actor &player, Rules rules) {
	int round = 0;
	int currentLength = rules.initialLength;
	bool playerIsEmperor;

	vector<Card> enemyDeck;
	vector<Card> playerDeck;
	enemy.deck = enemyDeck;
	player.deck = playerDeck;

	//Set this as the opposite due to modulo swap
	playerIsEmperor = !rules.playerIsEmperorFirst;

	//Game loop
	while (round < rules.maxRounds && currentLength > 0) {
		system("cls");
		//Phase 0 - Initialize
		bool roundOngoing = true;
		bool playerWon = false;
		if ((round % rules.swapCount) == 0) playerIsEmperor = playerIsEmperor ? false : true; //Swap
		DistributeCards(enemy.deck, rules.maxCards, !playerIsEmperor);
		DistributeCards(player.deck, rules.maxCards, playerIsEmperor);
		round++;

		//Phase 1 - Show Stats
		cout << "Cash: " << player.money << " " << rules.currencyType
			<< "\nDistance Left" << "(" << rules.unitType << "): " << currentLength
			<< "\nRound: " << round << "/" << rules.maxRounds
			<< "\nSide: " << (playerIsEmperor ? "Emperor" : "Slave")
			<< "\n=================="
			<< "\n" << endl;

		//Phase 2 - Wager
		cout << "\"How much would you like to wager, " << player.name << "?\" ";
		int wager = PickBetween(1, currentLength);

		//Phase 3 - Play a Round
		while (roundOngoing) {
			system("cls");
			cout << "You wagered " << wager << " " << rules.unitType
				<< "\n=================="
				<< "\n" << endl;

			//Phase 3.1 - Pick a card
			cout << "Pick a card to play: " << endl;
			ShowDeck(player.deck);
			int playerPick = PickBetween(1, player.deck.size());
			int aiPick = rand() % enemy.deck.size();
			playerPick--; //Reduce it by one

			//Phase 3.2 - Comparison
			system("cls");
			cout << "\"Hohoho...\""
				<< "\n==================" << endl;
			cout << player.name << " picked " << (CARD_NAMES[player.deck[playerPick].type]) << endl;
			PrintElipses(5, 200);
			cout << enemy.name << " picked " << (CARD_NAMES[enemy.deck[aiPick].type]) << endl;
			Sleep(400);

			cout << endl;
			cout << "=================="
				<< "\n\"UwU, What's this?\""
				<< "\n==================" << endl;
			Sleep(400);

			//Phase 3.4 - Result
			Result result;
			result = CompareTwoCards(player.deck[playerPick], enemy.deck[aiPick]);
			playerWon = (result == win) ? true : false;
			roundOngoing = (result == draw) ? true : false;
			if (result == draw) {
				cout << "It's a Draw!"
					<< "\nThe round continues..."
					<< "\n"
					<< "\n====NEXT CARD=====" << endl;
				system("pause");
			}

			//Phase 3.4 - Delete the picked card
			player.deck.erase(player.deck.begin() + (playerPick));
			enemy.deck.erase(enemy.deck.begin() + aiPick);
			//Card exchange end
		}

		//Phase 4 - Round Evaluation
		if (playerWon) {
			int winAmount = (wager * rules.payoutPerLength);
			winAmount *= playerIsEmperor ? 1 : rules.slaveMult;
			cout << player.name << " won the round!"
				<< "\n" << player.name << " won a total of " << winAmount << " " << rules.currencyType
				<< "\n" << player.name << " stuffed beans in their underpants"
				<< "\n\n====NEXT ROUND====" << endl;
			player.money += winAmount;
		}
		else {
			currentLength -= wager;
			cout << player.name << " lost the exchange!" << endl;
			if (currentLength > 0) {
				cout << player.name << " lost the round!"
					<< "\nThe contraption comes " << wager << " " << rules.unitType << " closer."
					<< "\n" << player.name << " ate some pizza to calm down..."
					<< "\n\n====NEXT ROUND====" << endl;
			}
			else {
				cout << player.name << " lost the round!"
					<< "\nThe contraption pierces through " << player.name << "'s left eardrum!"
					<< "\nSpaghetti falls out of " << player.name << "'s pockets."
					<< "\n\n=PAIN IS TOO MUCH=" << endl;
			}
		}

		//Phase 5 - Clear the deck
		player.deck.clear();
		enemy.deck.clear();
		system("pause");
		//Round end
	}

	//Phase 5 - Ending Evaluation
	system("cls");
	cout << "======ENDING======" << endl;
	//Worst Ending
	if (currentLength <= 0) {
		cout << player.name << " has " << player.money << " " << rules.currencyType << " but wasted their spaghetti!"
			<< "\nLost their left ear as well."
			<< "\n\"Ouchy Ouchy!\""
			<< "\n\"" << player.name << ", you alright?\""
			<< "\n\"Just follow the 5 second rule.\""
			<< "\n"
			<< "\n" << player.name << " got the Worst ending!" << endl;
		return;
	}

	//Best Ending
	if (player.money >= rules.victoryAmount) {
		cout << player.name << " has " << player.money << " " << rules.currencyType << "!"
			<< "\n\"H-Hmmph! It's not like I let you win or anything...\""
			<< "\n" << player.name << " threw a blueberry pie to " << enemy.name << "'s face"
			<< "\n\"" << player.name << ", you b-baka!\""
			<< "\n"
			<< "\n" << player.name << " got the Best ending!" << endl;
		return;
	}

	//Meh Ending
	cout << player.name << " has" << player.money << " " << rules.currencyType << "."
		<< "\n\"What?... You're not satisfied?\""
		<< "\n\"You'd rather have fried chicken that this?\""
		<< "\n\"" << player.name << "...\""
		<< "\n"
		<< "\n" << player.name << " got the Meh ending!" << endl;
	return;
	//Game end
}
#pragma once
#include <string>
class Shape
{
public:
	~Shape();
	virtual float GetArea();

	std::string GetName();
	int GetSides();

protected:
	std::string m_Name = "";
	int m_NumSides = 0;
};


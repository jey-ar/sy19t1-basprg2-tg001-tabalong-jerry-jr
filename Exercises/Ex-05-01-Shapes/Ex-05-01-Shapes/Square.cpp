#include "Square.h"



Square::Square(std::string name, int length)
{
	m_Name = name;
	m_Length = length;
	m_NumSides = 4;
}


Square::~Square()
{
}

float Square::GetArea()
{
	return (m_Length * m_Length);
}

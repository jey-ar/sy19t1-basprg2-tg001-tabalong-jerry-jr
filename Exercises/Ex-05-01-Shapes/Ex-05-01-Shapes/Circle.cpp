#include "Circle.h"



Circle::Circle(std::string name, int radius)
{
	m_Name = name;
	m_Radius = radius;
	m_NumSides = 0;
}


Circle::~Circle()
{
}

float Circle::GetArea()
{
	float pi = 3.14159f;
	float area = pi * (m_Radius * m_Radius);
	return area;
}

#pragma once
#include "Shape.h"
class Rectangle :
	public Shape
{
public:
	Rectangle(std::string name, int width, int height);
	~Rectangle();

	float GetArea() override;

private:
	int m_Width;
	int m_Height;
};


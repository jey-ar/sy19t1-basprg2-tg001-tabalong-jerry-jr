#pragma once
#include "Shape.h"
class Square :
	public Shape
{
public:
	Square(std::string name, int length);
	~Square();

	float GetArea() override;

private:
	int m_Length;
};


#include "Rectangle.h"



Rectangle::Rectangle(std::string name, int width, int height)
{
	m_Name = name;
	m_Width = width;
	m_Height = height;
	m_NumSides = 4;
}


Rectangle::~Rectangle()
{
}

float Rectangle::GetArea()
{
	return (m_Width * m_Height);
}

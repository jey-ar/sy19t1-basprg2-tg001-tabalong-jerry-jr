#pragma once
#include "Shape.h"
class Circle :
	public Shape
{
public:
	Circle(std::string name, int radius);
	~Circle();

	float GetArea() override;

private:
	int m_Radius;
};


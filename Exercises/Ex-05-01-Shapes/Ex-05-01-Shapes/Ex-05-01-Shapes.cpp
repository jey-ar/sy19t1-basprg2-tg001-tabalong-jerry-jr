// Ex-05-01-Shapes.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <vector>
#include "Rectangle.h"
#include "Circle.h"
#include "Square.h"

using namespace std;

void DisplayShape(Shape* shape) {
	cout << "Sides: " << shape->GetSides()
		<< "\nName: " << shape->GetName()
		<< "\nArea: " << shape->GetArea() << endl;
}

int main()
{
	Circle* circ = new Circle("Circle", 4);
	Rectangle* rect = new Rectangle("Rectangle", 4, 6);
	Square* squa = new Square("Square", 5);

	vector<Shape*> shapes;

	shapes.push_back(circ);
	shapes.push_back(rect);
	shapes.push_back(squa);

	for (auto a : shapes) {
		DisplayShape(a);
		cout << endl;
	}

	system("pause");
    return 0;
}
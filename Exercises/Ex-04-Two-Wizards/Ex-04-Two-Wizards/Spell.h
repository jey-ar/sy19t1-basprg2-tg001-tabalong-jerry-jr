#pragma once
#include <string>
class Spell
{
public:
	Spell(std::string name, int damage, int cost);
	std::string GetName();
	int GetDamage();
	int GetCost();
	virtual ~Spell();
private:
	std::string m_Name;
	int m_Damage, m_MpCost;
};
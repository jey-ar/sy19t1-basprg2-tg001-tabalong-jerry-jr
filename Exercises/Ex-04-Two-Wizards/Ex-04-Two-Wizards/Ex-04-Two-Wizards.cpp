#include <iostream>
#include "Wizard.h"

//Ex_04 Two Wizards

using namespace std;

void TwoWizardFight(Wizard * firstWiz, Wizard * secondWiz);

Spell Fireball("Fireball", 5, 1); //Global Spell

int main()
{
	Wizard* gandalf = new Wizard("Gandalf", 30, 10);
	Wizard* saruman = new Wizard("Saruman", 40, 5);

	gandalf->ShowStats();
	saruman->ShowStats();

	system("pause");
	system("cls");

	TwoWizardFight(gandalf, saruman);

	system("pause");
}

void TwoWizardFight(Wizard * firstWiz, Wizard * secondWiz)
{
	bool firstTurn = true;
	while (true) {
		if (firstTurn) {
			firstWiz->CastSpell(Fireball, *secondWiz);
			firstTurn = false;
		}
		else {
			secondWiz->CastSpell(Fireball, *firstWiz);
			firstTurn = true;
		}

		firstWiz->ShowStats();
		secondWiz->ShowStats();

		system("pause");
		cout << "--------------" << endl;

		//Check for deaths
		if (firstWiz->GetHealth() <= 0 || secondWiz->GetHealth() <= 0) {
			break;
		}
	}

	//Find winnder
	if (firstWiz->GetHealth() <= 0) {
		cout << secondWiz->GetName() << " wins!" << endl;
		delete firstWiz;
	}
	else {
		cout << firstWiz->GetName() << " wins!" << endl;
		delete secondWiz;
	}
}

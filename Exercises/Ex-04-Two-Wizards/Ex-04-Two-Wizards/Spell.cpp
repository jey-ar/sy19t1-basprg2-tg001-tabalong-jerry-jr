#include "Spell.h"



Spell::Spell(std::string name, int damage, int cost)
{
	m_Name = name;
	m_Damage = damage;
	m_MpCost = cost;
}

std::string Spell::GetName()
{
	return m_Name;
}

int Spell::GetDamage()
{
	return m_Damage;
}

int Spell::GetCost()
{
	return m_MpCost;
}

Spell::~Spell()
{
}

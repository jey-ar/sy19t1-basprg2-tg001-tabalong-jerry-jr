#pragma once
#include <iostream>
#include "Spell.h"
class Wizard
{
public:
	Wizard(std::string name, int health, int mana);
	std::string GetName();
	void SetName(std::string name);
	int GetHealth();
	void SetHealth(int value);
	int GetMana();
	void SetMana(int value);
	void CastSpell(Spell & spell, Wizard & target);
	void ShowStats();
	virtual ~Wizard();
private:
	std::string m_Name;
	int m_Health, m_Mana;
};

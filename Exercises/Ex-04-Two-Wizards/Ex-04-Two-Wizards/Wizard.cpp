#include "Wizard.h"



Wizard::Wizard(std::string name, int health, int mana)
{
	SetName(name);
	SetHealth(health);
	SetMana(mana);
}

std::string Wizard::GetName()
{
	return m_Name;
}

void Wizard::SetName(std::string name)
{
	m_Name = name;
}

int Wizard::GetHealth()
{
	return m_Health;
}

void Wizard::SetHealth(int value)
{
	if (value == 0) value = 0;
	m_Health = value;
}

int Wizard::GetMana()
{
	return m_Mana;
}

void Wizard::SetMana(int value)
{
	if (value < 0) value = 0;
	m_Mana = value;
}

void Wizard::CastSpell(Spell & spell, Wizard & target)
{
	if (GetMana() < spell.GetCost()) {
		std::cout << GetName() << " doesn't have enough Mana to cast " << spell.GetName() << " towards " << target.GetName() << std::endl;
		return;
	}
	SetMana(GetMana() - spell.GetCost());
	target.SetHealth(target.GetHealth() - spell.GetDamage());
	std::cout << GetName() << " casted a " << spell.GetName() << " towards " << target.GetName() << std::endl;
}

void Wizard::ShowStats()
{
	std::cout << GetName() << " [HP: " << GetHealth() << ", MP: " << GetMana() << "]" << std::endl;
}


Wizard::~Wizard()
{
}

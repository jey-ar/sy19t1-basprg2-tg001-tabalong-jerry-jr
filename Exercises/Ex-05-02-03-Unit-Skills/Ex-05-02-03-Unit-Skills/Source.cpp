#include <iostream>
#include <random>
#include <time.h>
#include "Unit.h"

using namespace std;

int main() {
	srand(time(NULL));

	Unit* Hero = new Unit("Hero", {10, 10, 5, 5, 5, 5});
	Hero->AddSkill(new Skill{ "Heal", Hero->GetStats().curHp, 10 });
	Hero->AddSkill(new Skill{ "Might", Hero->GetStats().pow, 2 });
	Hero->AddSkill(new Skill{ "Iron Skin", Hero->GetStats().vit, 2 });
	Hero->AddSkill(new Skill{ "Concentration", Hero->GetStats().dex, 2 });
	Hero->AddSkill(new Skill{ "Haste", Hero->GetStats().agi, 2 });

	while (true) {
		Hero->DisplayUnit();

		int randomSkill = rand() % Hero->GetSkillCount();
		Hero->GetSkill(randomSkill)->activate();
		cout << endl;

		system("pause");
		system("cls");
	}


	system("pause");
	return 0;
}
#include <iostream>
#include <string>

struct Skill {
	std::string name;
	int& statToInc;
	int buffAmt;

	void activate() {
		std::cout << name << " activated!";
		statToInc += buffAmt;
	}
};
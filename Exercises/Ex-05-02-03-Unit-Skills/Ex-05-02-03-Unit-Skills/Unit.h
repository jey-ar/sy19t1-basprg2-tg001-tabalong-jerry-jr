#pragma once
#include <iostream>
#include <vector>
#include "stats.cpp"
#include "skill.cpp"


class Unit
{
public:
	Unit(std::string name, Stats stats);
	~Unit();

	Stats& GetStats();
	
	void AddSkill(Skill* newSkill);
	Skill* GetSkill(int index);
	int GetSkillCount();
	void DisplayUnit();

private:
	std::string m_Name;
	Stats m_Stats;
	std::vector<Skill*> m_Skills;
};


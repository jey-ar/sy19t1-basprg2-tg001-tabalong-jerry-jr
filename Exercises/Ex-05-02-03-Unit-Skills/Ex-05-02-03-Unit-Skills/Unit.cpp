#include "Unit.h"


Unit::Unit(std::string name, Stats stats)
{
	m_Name = name;
	m_Stats = stats;
}


Unit::~Unit()
{
}

Stats & Unit::GetStats()
{
	return m_Stats;
}

void Unit::AddSkill(Skill* newSkill)
{
	m_Skills.push_back(newSkill);
}

int Unit::GetSkillCount()
{
	return m_Skills.size();
}

void Unit::DisplayUnit()
{
	std::cout << m_Name
		<< "\n\tHP:\t\t" << m_Stats.curHp
		<< "\n\tPower:\t\t" << m_Stats.pow
		<< "\n\tVitality:\t" << m_Stats.vit
		<< "\n\tDexterity:\t" << m_Stats.dex
		<< "\n\tAgility:\t" << m_Stats.agi
		<< std::endl;
}

Skill * Unit::GetSkill(int index)
{
	return m_Skills[index];
}

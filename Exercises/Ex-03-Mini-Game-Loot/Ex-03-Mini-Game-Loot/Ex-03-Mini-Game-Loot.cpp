#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include <windows.h>
#include <math.h>

using namespace std;

//Ex_03 Mini Game Loot

//Items Struct, name, value, cursed
struct Item {
	string name;
	int value; //value of the item
	bool cursed; //if true, bad things happen
};

//Loot Table Struct, item array, size, AddLoot Function, GenerateItem Function - 3-4
struct LootTable {
	Item* items; //Items in loot table
	int size; //size of the loot table

	//Add item in loot table
	void AddLoot(Item item) {
		if (item.name == "") return;
		for (int i = 0; i < size; i++) {
			//Check if empty in lootTable is empty
			if (items[i].name == "") {
				items[i].name = item.name;
				items[i].value = item.value;
				items[i].cursed = item.cursed;
				return;
			}
		}
		return;
	}

	//Generate a new random item in the lootTable - 3-4
	Item* GenerateItem() {
		int ran = rand() % size;
		Item* itemGenerated = new Item{ items[ran].name, items[ran].value, items[ran].cursed };
		return itemGenerated;
	}
};

//Create a loot table with an array given the size
LootTable CreateLootTable(int size) {
	LootTable lootTable = LootTable{ new Item[size], size };
	return lootTable;
}

//Standard Choice Checkers (Y/N) 
bool UserPressedY(char y = 'y', char n = 'n') {
	int choice = -1;
	while ((choice != y) && (choice != n)) {
		choice = _getch();
	}
	if (choice == y) {
		return true;
	}
	return false;
}

//print some dots
void PrintElipses(int amount, int waitTime = 200) {
	for (int i = 0; i < amount; i++) {
		cout << ". ";
		Sleep(waitTime);
	}
	cout << endl;
	return;
}

//Enter Dungeon deduct gold by cost, draw loot from lootTable - 3-5
void EnterDungeon(int& gold, int cost, LootTable lootTable, int goldCollected = 0, int round = 0) {
	system("cls");
	round++; //Increase Round
	gold -= cost;

	cout << "You opened Chest #" << round << " ";
	PrintElipses(3);

	Item* item = lootTable.GenerateItem();

	if (item->cursed) {
		cout << "You got a " << item->name << endl;
		Sleep(500);
		cout << "Oh! You tripped and fell unconcious.\n"
			<< "Next thing you know you are back at the entrance of the dungeon\n"
			<< "without your loot." << endl;
		goldCollected = 0;

		delete item; //Delete Item

		return;
	}

	goldCollected += item->value * round; //Multiplier based on round

	cout << "You got " << item->name << " valued at " << item->value * round << " gold.\n"
		<< "You collected a total of " << goldCollected << " gold.\n"
		<< "Explore the next room? (Y/N)" << endl;

	delete item;

	if (UserPressedY()) {
		//Run the function again but with no cost;
		return EnterDungeon(gold, 0, lootTable, goldCollected, round);
	}

	gold += goldCollected;
	cout << endl;
	cout << "You earned " << goldCollected << " gold.\n"
		<< "You now have a total of " << gold << " gold.\n"
		<< "You returned to the entrance of the dungeon and sold the loot." << endl;
	return;
}

//Returns an int based on the gold - 3-6
int EvaluateCondition(int& gold, int winAmount, int loseAmount) {
	if (gold < loseAmount) return 0; //Gameover
	if (gold < winAmount) return 1; //Try again
	return 2; //Win
}

int main() {
	srand(time(NULL));

	//Game conditions
	const int WIN_CONDITION = 500;
	const int DUNGEON_COST = 25;

	//Create array inside lootTable
	LootTable lootTable = CreateLootTable(5);

	//Add items in the in lootTable
	lootTable.AddLoot({ "Mithril Ore" , 100, false});
	lootTable.AddLoot({ "Sharp Talon" , 50, false });
	lootTable.AddLoot({ "Thick Leather" , 25, false });
	lootTable.AddLoot({ "Jellopy" , 5, false });
	lootTable.AddLoot({ "Cursed Stone" , 0, true });

	int playerGold = 50;
	bool gameIsOngoing = true;
	cout << "You have " << playerGold << " gold, you paid a fee of " << DUNGEON_COST << " gold to enter the dungeon." << endl;
	cout << "You stepped inside the dungeon ";
	PrintElipses(3, 1000);
	system("cls");
	
	while (gameIsOngoing) {

		//Enter dungeon until player gets a cursed item or quits
		EnterDungeon(playerGold, DUNGEON_COST, lootTable);

		system("pause");
		system("cls");

		switch (EvaluateCondition(playerGold, WIN_CONDITION, DUNGEON_COST)) {
		case 0: //Lost
			cout << "You have " << playerGold << " gold.\n"
				<< "You don't have enough gold to pay the " << DUNGEON_COST << " fee.\n"
				<< "You lost." << endl;
			gameIsOngoing = false; //End game
			break;
		case 1: //Play Again
			cout << "You now have " << playerGold << " gold.\n"
				<< "You need " << WIN_CONDITION - playerGold << " more gold.\n"
				<< "You went back inside the dungeon... and paid " << DUNGEON_COST << " gold." << endl;
			system("pause");
			break;
		case 2: //Win
			cout << "You have " << playerGold << " gold\n"
				<< "You win!" << endl;
			gameIsOngoing = false; //End game
			break;
		}
	}

	delete[] lootTable.items; //Destroy LootTable array
	system("pause");
	return 0;
}
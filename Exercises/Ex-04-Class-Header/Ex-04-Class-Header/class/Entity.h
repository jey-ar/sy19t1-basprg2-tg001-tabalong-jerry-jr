#pragma once
#include <string>
#include "sprite.h"

//Parent Class
class Entity
{
public:
	std::string mName;
	std::string mDescription;
	Sprite mPortrait;

	bool isActor, isEquipment, isOther;

	Entity();
	virtual ~Entity();
};


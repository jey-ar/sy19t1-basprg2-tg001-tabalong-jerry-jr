#pragma once
#include "Entity.h"

class Limit :
	public Entity
{
public:
	bool availOnStart, hasLearned;
	int minKills, minLimitBreaks;

	Limit();
	virtual ~Limit();
};


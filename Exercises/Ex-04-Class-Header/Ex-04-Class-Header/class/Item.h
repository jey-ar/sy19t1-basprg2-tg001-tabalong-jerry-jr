#pragma once
#include "Entity.h"

class Item :
	public Entity
{
public:
	bool isEquipment, isConsumable, isQuestItem;
	int value;
	Item();
	virtual ~Item();
};


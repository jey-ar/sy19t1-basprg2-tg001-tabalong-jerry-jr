#pragma once
#include "Item.h"
#include "Materia.h"

class Equipment :
	public Item
{
public:
	bool isWeapon, isArmor, isAccessory;

	int mHpMod, mMpMod;
	int mStrengthMod, mDexterityMod, mVitalityMod, mMagicMod, mSpiritMod, mLuckMod;
	int mAttackMod, mAttackPercMod, mDefenseMod, mDefensePercMod, mMagicAtkMod, mMagicDefMod, mMagicDefPercMod;

	bool allowMateria;
	int mMateriaGrowth;
	Materia mMatPair1Slot1, mMatPair1Slot2;
	Materia mMatPair2Slot1, mMatPair2Slot2;
	Materia mMatPair3Slot1, mMatPair3Slot2;
	Materia mMatPair4Slot1, mMatPair4Slot2;

	Equipment();
	void RemoveMatria();
	void EquipMateria();
	void StackStats();
	virtual ~Equipment();
};


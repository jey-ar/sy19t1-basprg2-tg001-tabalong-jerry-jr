#pragma once
#include "Entity.h"
#include "Equipment.h"
#include "Limit.h"

class Actor :
	public Entity
{
public:
	int mLvl, mCurExp, mNextExp;
	int mHp, mMp;
	int mStrength, mDexterity, mVitality, mMagic, mSpirit, mLuck;
	int mAttack, mAttackPerc, mDefense, mDefensePerc, mMagicAtk, mMagicDef, mMagicDefPerc;
	int mLimLvl;
	Limit mLimit1, mLimit2, mLimit3, mLimit4;
	Equipment mWeapon, mArmor, mAccessory;


	Actor();
	void Move();
	void Attack();
	void Equip();
	void Talk();
	void Sell();

	void Update();
	virtual ~Actor();
};


// Ex-04-Class-Header.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "class/Sprite.h"
#include "class/Actor.h"

int main()
{
	Actor* cloud = new Actor(); cloud->mName = "Cloud";

    std::cout << "Hello " << cloud->mName << "! \n"; 
	system("pause");
	return 0;
}

#include <iostream>
#include <string>
#include <time.h>

using namespace std;

//Ex_02 Simple Dice Game
//Roll struct
struct Roll {
	int value = 0; //sum of the 2 dice, value of 99 is Snake Eyes
	int dice[2] = { 0, 0}; //The dice array 
	int size = 2; //size of dice
};

//Player struct
struct Player {
	int currentGold; //gold in person
	int bet; //bet of the player
	Roll roll;
};

//Asks player for a bet and deducts gold immediately - 2-1
//returns bet
int GetPlayerBet(int& playerGold) {
	int bet = -1;
	cout << "What is your bet?";
	//If bet is not or less than 0 and not greater than player gold
	while (bet <= 0 || bet > playerGold) {
		cin >> bet;
	}
	//reduce playerGold with bet
	playerGold -= bet;
	return bet;
}

//Asks player for a bet and deducts gold immediately - 2-1
//doesn't return a value, needs the bet as a parameter 
void GetPlayerBet(int& playerGold, int& bet) {
	cout << "What is your bet? ";
	//If bet is not or less than 0 and not greater than player gold
	while (bet <= 0 || bet > playerGold) {
		cin >> bet;
	}

	//reduce playerGold with bet
	playerGold -= bet;
	return;
}

//Rolls the Dice and evaluates immediately what it has 2-2
void RollDice(Roll& roll) {
	//Roll amount of dice based on size
	for (int i = 0; i < roll.size; i++) {
		roll.dice[i] = (rand() % 6) + 1;
		cout << roll.dice[i] << " ";
	}
	//Get SnakeEyes
	if (roll.dice[0] == roll.dice[1] && roll.dice[0] == 1) {
		roll.value = 99;
		return;
	}

	//Get Value 
	roll.value = roll.dice[0] + roll.dice[1];
	return;
}

//Compare dice and output an integer for Ppayout function, 0 - Draw, 1 - AI Win, 2 - Player Win.
//First parameter must be the aiRoll
int CompareDice(Roll& aiRoll, Roll& playerRoll) {
	if (aiRoll.value == playerRoll.value) {
		cout << aiRoll.value << " = " << playerRoll.value;
		return 0;
	}
	if (aiRoll.value > playerRoll.value) {
		cout << aiRoll.value << " > " << playerRoll.value;
		return 1;
	}
	else {
		cout << aiRoll.value << " < " << playerRoll.value;
		return 2;
	}
}

//Payout only for Human Player - 2-3
void Payout(Player& humanPlayer, int result) {
	if (result == 0) {
		cout << "Draw - Player gets back their bet.";
		humanPlayer.currentGold += humanPlayer.bet;
		return;
	}

	if (result == 1) {
		cout << "Lose - Player loses their bet for nothing";
		return;
	}
	else {
		//Check for snakeyes
		if (humanPlayer.roll.value == 99) {
			cout << "Win + Snake Eyes - Player gets back thrice their bet.";
			humanPlayer.currentGold += (humanPlayer.bet * 3);
			return;
		}
		else {
			cout << "Win - Player wins amount equal to their bet.";
			humanPlayer.currentGold += (humanPlayer.bet * 2);
			return;
		}
	}
}

//Plays a round until humanPlayer runs out of gold - 2-4
//First parameter must be the aiPlayer
void PlayRound(Player& aiPlayer, Player& humanPlayer) {
	//Phase -1 - Reset values and clear screen
	system("cls");
	aiPlayer.roll = { 0, {0, 0}, 2 };
	humanPlayer.roll = { 0, {0, 0}, 2 };
	humanPlayer.bet = 0; 

	//Phase 0 - Check Gold
	if (humanPlayer.currentGold <= 0) {
		return; //exit Function
	}

	//Phase 1 - Get Bet
	cout << "Player have " << humanPlayer.currentGold << " Gold" << endl; 
	cout << "---------------------" << endl;
	GetPlayerBet(humanPlayer.currentGold, humanPlayer.bet);

	//Phase 2 - AI Rolls first
	cout << "---------------------" << endl;
	cout << "AI Rolls" << endl;
	RollDice(aiPlayer.roll);
	cout << "// Value is: "<< aiPlayer.roll.value << endl;
	cout << endl;
	system("pause");

	//Phase 3 - Player Rolls second
	cout << "---------------------" << endl;
	cout << "Player Rolls" << endl;
	RollDice(humanPlayer.roll);
	cout << "// Value is: " << humanPlayer.roll.value << endl;
	cout << endl;
	system("pause");

	//Phase 4 - Compare rolls
	cout << "---------------------" << endl;
	int result;
	result = CompareDice(aiPlayer.roll, humanPlayer.roll);
	cout << endl;
	system("pause");

	//Phase 5 - Payout based on compare
	cout << "---------------------" << endl;
	Payout(humanPlayer, result);
	cout << endl;
	system("pause");

	//Play again
	return PlayRound(aiPlayer, humanPlayer);
}

int main()
{
	//Initialization
	srand(time(NULL)); //Randomize Seed
	Player robotPlayer; 
	Player thePlayer = { 1000 }; //1000 starting gold

	//Play until player has no gold left
	PlayRound(robotPlayer, thePlayer);

	cout << "Game Over - Player has no Gold left" << endl;
	system("pause");
	cout << endl;
	return 0;
}


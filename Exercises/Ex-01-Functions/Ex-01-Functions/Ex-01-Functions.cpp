#include <iostream>
#include <string>
#include <time.h>

using namespace std;

//Print Factorial, Procedure and Output - 1-1
void PrintFactorial(int number) {
	if (number < 0) return; //No output on negative
	if (number == 0) { 
		cout << "0! = 1" << endl;
		return;
	}

	int output = 1;
	cout << number << "! = ";
	for (int i = number; i > 0; i--) {
		output = output * i;
		cout << i;

		if (i > 1) { //no 'x' on last number
			cout << " x ";
		}

	}
	cout << " = " << output;
	return;
}

//Print String and Int Array - 1-2
void PrintArray(string items[], int size) {

	for (int i = 0; i < size; i++) {
		cout << items[i] << " / ";
	}
	return;
}
void PrintArray(int items[], int size) {

	for (int i = 0; i < size; i++) {
		cout << items[i] << " ";
	}
	return;
}

//Find number of instances in array - 1-3
void FindInstancesArray(string items[], int size) {
	string itemToFind;
	int count = 0;

	cout << "What would you like to find? ";
	getline(cin, itemToFind);

	for (int i = 0; i < size; i++) {
		if (itemToFind == items[i]) {
			count++;
		}
	}

	if (count > 0) {
		cout << "There are " << count << " instances of " << itemToFind;
	}
	else {
		cout << "There are no instances of" << itemToFind;
	}
	return;
}

//Fill Array with integer randomly - 1-4
void FillRandomArray(int numbers[], int size) {
	for (int i = 0; i < size; i++) {
		int random = (rand() % 100) + 1; //1-100
		numbers[i] = random;
	}
	return;
}

//Output the largest number in an array - 1-5
int FindLargestNumber(int numbers[], int size) {
	int largest = numbers[0]; //Store first number

	for (int i = 0; i < size; i++) {		
		if (largest < numbers[i]) {
			largest = numbers[i];
		}
	}

	return largest;
}

//Sort an int array in ascending order - 1-6
void SortIntArrayAsc(int numbers[], int size) {
	for (int i = 0; i < size; i++) {

		int check = numbers[i];
		int swapIndex = i;

		for (int ii = i; ii < size; ii++) {
			//Find number greater than check then store that
			if (numbers[ii] < check) {
				check = numbers[ii];
				swapIndex = ii;
			}
		}

		//If check is not equal to array[i] start swapping
		if (numbers[i] != check) {
			numbers[swapIndex] = numbers[i];
			numbers[i] = check;
		}
	}
	return;
}

int main()
{
	srand(time(NULL));
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	
	cout << "1. Print Factorial:" << endl;
	PrintFactorial(10);
	cout << endl;

	cout << "---------------------" << endl;
	cout << "2. Print an Array:" << endl;
	PrintArray(items, 8);
	cout << endl;

	cout << "---------------------" << endl;
	cout << "3. Find Instances of an element in Array #2:" << endl;
	FindInstancesArray(items, 8);
	cout << endl;

	cout << "---------------------" << endl;
	cout << "4. Fill an Array with Random Numbers (1-100):" << endl;
	int intArray[10];
	FillRandomArray(intArray, 10);
	PrintArray(intArray, 10);
	cout << endl;

	cout << "---------------------" << endl;
	cout << "5. Find the largest number in Array #4:" << endl;
	int largest = FindLargestNumber(intArray, 10);
	cout << "The largest number is: " << largest;
	cout << endl;

	cout << "---------------------" << endl;
	cout << "6. Sort Array #4 in Ascending order:" << endl;
	SortIntArrayAsc(intArray, 10);
	PrintArray(intArray, 10);
	cout << endl;

	system("pause");
	return 0;
}
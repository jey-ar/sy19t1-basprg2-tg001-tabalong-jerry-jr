#include <iostream>
#include <string>
#include <time.h>

using namespace std;

//Fill array with random values - 3-1
void FillArray(int* numbers, int size) {
	for (int i = 0; i < size; i++) {
		numbers[i] = rand() % 101;
	}
	return;
}

//Print Array
void PrintArray(int numbers[], int size) {
	for (int i = 0; i < size; i++) {
		cout << numbers[i] << " ";
	}
	return;
}

//Create Int Array based on size, and fill with random values - 3-2
int* CreateArray(int size) {
	int* newArray = new int[size];
	FillArray(newArray, size);
	return newArray;
}

//Create dynamically allocated array and integers then delete - 3-3
void CreateDeleteArray(int size) {
	int* newArray = new int[size];
	delete[] newArray;
}

int main()
{
	int test[5];
	srand(time(NULL));

	FillArray(test, 5);
	PrintArray(test, 5);
	cout << endl;

	int* dynamicArray = CreateArray(15);
	PrintArray(dynamicArray, 15);
	cout << endl;

	CreateDeleteArray(20);

	system("pause");
}

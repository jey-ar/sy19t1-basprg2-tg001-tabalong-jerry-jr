#include "Utils.h"

void SetColor(color color) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo(hConsole, &info);
	SetConsoleTextAttribute(hConsole, color);
}

void SetColor(int color) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo(hConsole, &info);
	SetConsoleTextAttribute(hConsole, color);
}

int GetColor() {
	CONSOLE_SCREEN_BUFFER_INFO info;
	if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &info))
		return 0;
	return info.wAttributes;
}

void PrintEllipses(int amt, int wait)
{
	for (int i = 0; i < amt; i++) {
		std::cout << ". ";
		Sleep(wait);
	}
	std::cout << std::endl;
	return;
}

int PickBetween(int min, int max)
{
	int n = INT_MIN;
	while (n < min || n > max) {
		std::cin >> n;
		if (n < min || n > max) {
			std::cin.clear();
			std::cin.ignore(INT_MAX, '\n');
		}
	}
	return n;
}

void Clamp(int & value, int min, int max)
{
	if (value < min) value = min;
	if (value > max) value = max;
}

void Clamp(float & value, float min, float max)
{
	if (value < min) value = min;
	if (value > max) value = max;
}

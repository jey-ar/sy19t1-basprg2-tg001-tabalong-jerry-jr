#pragma once
#include <stdexcept>
#include <string>
#include <iostream>
#include <math.h>
#include "Utils.h"

const std::string ClassNames[5] = { "Warrior", "Assassin", "Mage", "Peasant", "God" };
enum ClassType { WarriorType, AssassinType, MageType, PeasantType, Debug, MAX_CLASSES = 5};
enum ActorValue {level, health, maxHealth, power, vitality, agility, dexterity};

class Actor
{
public:
	Actor(std::string name = "");
	virtual ~Actor();
	std::string GetName();
	ClassType GetClass();
	std::string GetNameClass();
	ClassType GetWeakness();

	void SetActorValue(ActorValue actorValue, int value);
	int GetActorValue(ActorValue actorValue);
	void ModActorValue(ActorValue actorValue, int value);
	void SetAV(ActorValue actorValue, int value);
	int GetAV(ActorValue actorValue);
	void ModAV(ActorValue actorValue, int value);

	void KilledBy(Actor * actor);
	Actor* GetKiller();

	bool Attack(Actor * actor);
	void HealSelf(int value);
	void ShowStats();
	
protected:
	void SetClass(ClassType classType);
	void SetWeakness(ClassType classType);
	void SetName(std::string name);

private:
	const int MIN_HITRATE = 20, MAX_HITRATE = 80;
	const int MIN_DAMAGE = 1, MAX_DAMAGE = INT_MAX;
	const float WEAKNESS_MULT = 1.5f;

	std::string m_Name;
	ClassType m_Class, m_Weakness;
	Actor * m_Killer = nullptr;
	int m_Level, m_Health, m_MaxHealth, m_Power, m_Vitality, m_Agility, m_Dexterity;
	void ShowHealthBar(float perc, int size = 1);
};
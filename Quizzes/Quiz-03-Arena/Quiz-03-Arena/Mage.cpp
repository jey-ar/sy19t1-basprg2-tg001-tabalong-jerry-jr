#include "Mage.h"


Mage::Mage(std::string name, int lvl)
{
	SetName(name);
	SetClass(MageType);
	SetWeakness(AssassinType);

	SetActorValue(level, lvl);
	SetActorValue(health, 15 + (0.5f * lvl));
	SetActorValue(maxHealth, 15 + (0.5f * lvl));
	SetActorValue(power, 15 + (1.1f * lvl));
	SetActorValue(vitality, 5 + (0.5f * lvl));
	SetActorValue(agility, 10 + (0.9f * lvl));
	SetActorValue(dexterity, 8 + (0.7f * lvl));
}

Mage::~Mage()
{	
	GetKiller()->ModActorValue(power, 5);
}

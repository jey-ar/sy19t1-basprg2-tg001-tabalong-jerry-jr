#include "Actor.h"

Actor::Actor(std::string name)
{
	SetName(name);
	SetClass(PeasantType);
	SetWeakness(PeasantType);
	
	SetActorValue(level, 1);
	SetActorValue(health, 5);
	SetActorValue(maxHealth, 5);
	SetActorValue(power, 0);
	SetActorValue(vitality, 0);
	SetActorValue(agility, 5);
	SetActorValue(dexterity, 5);
}

Actor::~Actor()
{
}

void Actor::SetName(std::string name)
{
	m_Name = name;
}

std::string Actor::GetName()
{
	if (this == nullptr) return "";
	return m_Name;
}

ClassType Actor::GetClass()
{
	return m_Class;
}

std::string Actor::GetNameClass()
{
	if ((int)m_Class >= 0 && (int)m_Class < (int)MAX_CLASSES) return ClassNames[(int)m_Class];
	else throw std::invalid_argument("Class doesn't exist.");
}

ClassType Actor::GetWeakness()
{
	return m_Weakness;
}

void Actor::SetActorValue(ActorValue actorValue, int value)
{
	if (this == nullptr) return;
	if (value < 0) value = 0;
	switch (actorValue) {
	case level: m_Level = value; break;
	case health: m_Health = value; break;
	case maxHealth: m_MaxHealth = value; break;
	case power: m_Power = value; break;
	case vitality: m_Vitality = value; break;
	case dexterity: m_Dexterity = value; break;
	case agility: m_Agility = value; break;
	default: break;
	}
}

int Actor::GetActorValue(ActorValue actorValue)
{
	if (this == nullptr) return 0;
	switch (actorValue) {
	case level: return m_Level; break;
	case health: return m_Health; break;
	case maxHealth: return m_MaxHealth; break;
	case power: return m_Power; break;
	case vitality: return m_Vitality; break;
	case dexterity: return m_Dexterity; break;
	case agility: return m_Agility; break;
	default: return 0; break;
	}
}

void Actor::ModActorValue(ActorValue actorValue, int value)
{
	SetActorValue(actorValue, GetActorValue(actorValue) + value);
}

void Actor::SetAV(ActorValue actorValue, int value)
{
	SetActorValue(actorValue, value);
}

int Actor::GetAV(ActorValue actorValue)
{
	return GetActorValue(actorValue);
}

void Actor::ModAV(ActorValue actorValue, int value)
{
	ModActorValue(actorValue, value);
}

void Actor::KilledBy(Actor * actor)
{
	if (actor != nullptr) m_Killer = actor;
}

Actor* Actor::GetKiller()
{
	if (m_Killer != nullptr) return m_Killer;
	else return nullptr;
}

bool Actor::Attack(Actor * target)
{
	float hitRate = ((float)this->GetAV(dexterity) / (float)target->GetAV(dexterity)) * 100;
	Clamp(hitRate, MIN_HITRATE, MAX_HITRATE);

	int rng = rand() % 101; // 0 - 100
	std::cout << "(" << rng << "%) ";
	if (hitRate < rng) {
		std::cout << this->GetName() << " missed " << target->GetName() << "!" << std::endl;
		return false; //MISS
	}
	
	int damage = (this->GetAV(power) - target->GetAV(vitality));
	int colb4 = GetColor();
	if (this->GetClass() == target->GetWeakness()) {
		SetColor(yellow); //Color yellow for the super effective hit
		damage *= WEAKNESS_MULT;
	}
	Clamp(damage, MIN_DAMAGE, MAX_DAMAGE);

	target->ModAV(health, -damage);
	std::cout << this->GetName() << " hit " << target->GetName() << " for " << damage << " damage!" << std::endl;
	SetColor(colb4);
	if (target->GetAV(health) <= 0) {
		std::cout << target->GetName() << " is defeated." << std::endl;
		target->KilledBy(this);
	}
	return true; //HIT
}

void Actor::HealSelf(int value)
{
	if (m_Health + value > m_MaxHealth) SetActorValue(health, m_MaxHealth);
	else SetActorValue(health, m_Health + value);
}

void Actor::ShowHealthBar(float perc, int size)
{

	std::cout << "[";
	int beforeColor = GetColor();
	if (perc >= 8) SetColor(green);
	else if (perc < 8 && perc > 3) SetColor(yellow);
	else SetColor(red);
	int bars = 0;
	for (int i = 0; i < perc * size; i++) {
		std::cout << "=";
		bars++;
	}
	for (int i = 0; i < (10 * size) - bars; i++) {
		std::cout << " ";
	}
	SetColor(beforeColor);
	std::cout << "] ";
}

void Actor::SetClass(ClassType classType)
{
	if (classType == MAX_CLASSES) throw std::invalid_argument("Not supposed to get that...");
	else m_Class = classType;
}

void Actor::SetWeakness(ClassType classType)
{
	if (classType == MAX_CLASSES) throw std::invalid_argument("Not supposed to get that...");
	else m_Weakness = classType;
}


void Actor::ShowStats()
{
	float healthPerc = ((float)m_Health / (float)m_MaxHealth) * 10;
	std::cout << m_Name << "\t(" << GetNameClass() << ")" << std::endl;
	ShowHealthBar(healthPerc, 2);
	std::cout << m_Health << " / " << m_MaxHealth << std::endl;

	std::cout << "Power: " << m_Power << "\tVitality: " << m_Vitality
		<< "\nAgility: " << m_Agility << "\tDexterity: " << m_Dexterity
		<< std::endl;
}
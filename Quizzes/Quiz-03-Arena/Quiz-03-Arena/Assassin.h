#pragma once
#include "Actor.h"
class Assassin :
	public Actor
{
public:
	Assassin(std::string name, int level);
	virtual ~Assassin();
};


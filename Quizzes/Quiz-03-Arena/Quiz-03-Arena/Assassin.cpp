#include "Assassin.h"


Assassin::Assassin(std::string name, int lvl)
{
	SetName(name);
	SetClass(AssassinType);
	SetWeakness(WarriorType);

	SetActorValue(level, lvl);
	SetActorValue(health, 15 + (0.7f * lvl));
	SetActorValue(maxHealth, 15 + (0.7f * lvl));
	SetActorValue(power, 10 + (0.9f * lvl));
	SetActorValue(vitality, 3 + (0.5f * lvl));
	SetActorValue(agility, 15 + (0.9f * lvl));
	SetActorValue(dexterity, 15 + (0.9f * lvl));
}

Assassin::~Assassin()
{
	GetKiller()->ModActorValue(agility, 3);
	GetKiller()->ModActorValue(dexterity, 3);
}

#include "Warrior.h"


Warrior::Warrior(std::string name, int lvl)
{
	SetName(name);
	SetClass(WarriorType);
	SetWeakness(MageType);

	SetActorValue(level, lvl);
	SetActorValue(health, 30 + (0.9f * lvl));
	SetActorValue(maxHealth, 30 + (0.9f * lvl));
	SetActorValue(power, 8 + (0.9f * lvl));
	SetActorValue(vitality, 6 + (0.8f * lvl));
	SetActorValue(agility, 7 + (0.5f * lvl));
	SetActorValue(dexterity, 10 + (0.8f * lvl));
}

Warrior::~Warrior()
{
	GetKiller()->ModActorValue(maxHealth, 3);
	GetKiller()->ModActorValue(vitality, 3);
}

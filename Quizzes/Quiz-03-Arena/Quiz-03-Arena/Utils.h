#pragma once
#include <windows.h>
#include <stdexcept>
#include <iostream>

enum color {green = 10, blue = 11 , red = 12, pink = 13, yellow = 14, white = 15};

void SetColor(color color);
void SetColor(int color);
int GetColor();
void PrintEllipses(int amt, int wait = 200);
int PickBetween(int min, int max);
void Clamp(int & value, int min, int max);
void Clamp(float & value, float min, float max);
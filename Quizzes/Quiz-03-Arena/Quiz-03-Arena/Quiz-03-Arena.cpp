#include <iostream>
#include <time.h>
#include <windows.h>

#include "Warrior.h"
#include "Assassin.h"
#include "Mage.h"
#include "Utils.h"

//Quiz_03 Arena

using namespace std;

const float HEAL_MULT = 0.3f;
Actor* CreateCharacter(bool random = false, int lvl = 1);
void EnterTheArena(Actor * player, Actor * enemy, int round);

int main()
{
	srand(time(nullptr));

	Actor* player = CreateCharacter();
	Actor* enemy = new Actor();

	int round = 0;
	while (player->GetAV(health) > 0) { 
		round++;
		enemy = CreateCharacter(true, round);
		EnterTheArena(player, enemy, round);
	}
	//round - 1, since player didn't survive the current round
	cout << "==============GAME END============="
		<< "\nYou survived for " << round - 1 << " rounds."
		<< "\nKilled by " << player->GetKiller()->GetName() << "."
		<< "\nYour stats: " << endl;
	player->ShowStats(); cout << endl;
	system("pause");

	if (enemy != nullptr) delete enemy;
	delete player;

	return 0;
}

Actor* CreateCharacter(bool random, int lvl) {
	string name;
	int classChoice;

	Actor* actor;
	bool debug = false;
	if (!random) {	
		cout << "=======Create your Character======="
			<< "\nWhat is your name? ";
		getline(cin, name);
		if (name == "debug") debug = true;
		cout << name << ", what do you want to be?"
			<< "\n\t[0] - Warrior" 
			<< "\n\t[1] - Assassin" 
			<< "\n\t[2] - Mage"
			<< "\n\t[3] - \"I don\'t know\""<< endl;
		classChoice = PickBetween(0, (int)ClassType::MAX_CLASSES - (2 - debug));
	}
	else {
		classChoice = rand() % 3;
		name = ClassNames[classChoice] + " " + to_string(lvl);
	}
	
	switch (classChoice) {
	case 0: actor = new Warrior(name, lvl); break;
	case 1: actor = new Assassin(name, lvl); break;
	case 2: actor = new Mage(name, lvl); break;
	case 3: actor = new Actor(name); break;
	case 4: actor = new Mage(name, 999); break;
	default: actor = new Actor(name); break;
	}

	if (!random) {
		switch (classChoice) {
		case 3: cout << "Only a weakling doesn't know what they want in life..." << endl; break;
		default: cout << "You picked " << actor->GetNameClass() << "." << endl; break;
		}
		system("pause");
		system("cls");
	}

	return actor;
}

void EnterTheArena(Actor * player, Actor * enemy, int round) {
	int turn = 0;
	bool playerTurn; 
	playerTurn = (player->GetAV(agility) >= enemy->GetAV(agility)) ? true : false;

	while (true) {		
		system("cls");
		turn++;
		cout << "===============FIGHT==============="
			<< "\nRound: " << round << "\tTurn: " << turn << endl;	
		player->ShowStats();
		int colb4 = GetColor();
		SetColor(red); cout << "\n     --------VS--------\n" << endl; SetColor(colb4);
		enemy->ShowStats(); cout << endl;

		PrintEllipses(5, 100);

		if (playerTurn) player->Attack(enemy);
		else enemy->Attack(player);
		//Swap
		playerTurn = !playerTurn; 

		system("pause");

		if (player->GetAV(health) <= 0) {
			cout << enemy->GetName() << " Won!" << endl;
			break;
		}
		else if (enemy->GetAV(health) <= 0) {
			cout << player->GetName() << " Won!" << endl;		

			switch (enemy->GetClass()) {
			case WarriorType: cout << player->GetName() << "'s Health and Vitality has increase by 3!" << endl; break;
			case MageType: cout << player->GetName() << "'s Power has increase by 5!" << endl; break;
			case AssassinType: cout << player->GetName() << "'s Agility and Dexterity has increase by 3!" << endl; break;
			default: cout << "What?" << endl;
			}

			delete enemy; enemy = nullptr;

			int healAmount = player->GetAV(maxHealth) * HEAL_MULT;
			//Get Color Before then Set Green
			colb4 = GetColor(); SetColor(green); 
			cout << player->GetName() << " is healed by " << healAmount << " heatlth." << endl;
			SetColor(colb4);
			//Heal player
			player->HealSelf(healAmount); 
			cout << player->GetName() << " continued walking forward..." << endl;

			break;
		}	
	}
	system("pause");
	system("cls");
}
#pragma once
#include "Actor.h"
class Warrior :
	public Actor
{
public:
	Warrior(std::string name, int level);
	virtual ~Warrior();
};


#include <iostream>
#include <time.h>
#include "node.h"

using namespace std;

//Quiz_2 The Wall

//List stores only the head and tail Nodes. In between is dependent on the head/tail.
struct List {
	Node* head = NULL;
	Node* tail = NULL;

	//Make a new Node
	void AddNode(Node* node) {		
		Node* newNode = node;

		//Set first node in the list as head
		if (head == NULL) {
			head = newNode;
			return;
		}
		//Set second node to add in the list as the tail
		else if (tail == NULL) {
			head->next = newNode; //head now points to the newNode
			head->previous = newNode; 
			newNode->next = head; //newNode points to head
			newNode->previous = head;
			tail = newNode; //set the newNode as the tail
			return;
		}
		//Every Succeeding nodes will replace tail
		else {
			head->previous = newNode; //head previous points to newNode
			tail->next = newNode; //the tail points to newNode;
			newNode->previous = tail; //newNode sets tail as its previous
			newNode->next = head;//newNode sets head as its next
			tail = newNode; //newNode now becomes the tail
			return;
		}
	}

	//Delete a node and connect inbetween
	void DeleteNode(Node* nodeToDelete) {
		//Connect the nodeToDelete's next and previous nodes together
		nodeToDelete->next->previous = nodeToDelete->previous;
		nodeToDelete->previous->next = nodeToDelete->next;

		delete nodeToDelete;
		nodeToDelete = NULL;
		return;
	}

	//Set a new head
	void SetHead(Node* node) {
		head = node;
		tail = node->previous;
		return;
	}

	//Print everything in the list until it points back to head
	void PrintList(bool withIndent = true) {
		if (head == NULL) return; 

		Node* printNode = head; //Print head first
		do {
			if (withIndent) cout << "\t"; //Enable indentation aesthetic
			cout << printNode->name << endl; //Print name of Node
			//Check if there is only one node
			if (printNode->next != NULL) {
				printNode = printNode->next; //Set the next one to print
			}	
		} while (printNode != head); //Stop when it is the head again
		return;
	}

	//Count how many Nodes in the list, return an int
	int CountList() {
		int count = 0;
		if (head == NULL) return count;
		//Same as PrintList();
		Node* countNode = head;
		do {
			count++;
			if (countNode->next != NULL) {
				countNode = countNode->next;
			}
		} while (countNode != head);
		return count;
	}

	//Randomly set a new head based on number of Nodes in list
	void RandomlySetNewHead() {
		if (CountList() == 0) return; //Avoid divison by zero if there are no nodes
		int newHeadCount = (rand() % CountList());
		while (newHeadCount > 0) {
			SetHead(head->next); //Set next of head as the head;
			//SetNextOfHeadAsHead();
			newHeadCount--;
		}
		return;
	}

	//Print everything - For Debug
	void PrintListExtensive(bool withIndent = true) {
		if (head == NULL) return;

		cout << "head: " << head->name << endl;
		cout << "tail: " << tail->name << endl;

		Node* printNode = head;
		do {
			if (withIndent) cout << "\t";
			cout << printNode->name
				<< "; Next: " << printNode->next->name
				<< "; Prev: " << printNode->previous->name << endl;
			printNode = printNode->next;
		} while (printNode != head);
		return;
	}
};

//Play the Game of Ice Water with the selected player list
void PlayGame(List& players) {
	int round = 1;

	//Get count of initial players
	int totalPlayers = players.CountList();
	if (totalPlayers == 0) return;

	while (round < totalPlayers) {
		cout << "=================" << endl;
		cout << "ROUND " << round << endl;
		cout << "=================" << endl;		
		cout << endl;

		cout << "Remaining Members:" << endl;
		players.PrintList(); 

		//Find how many is left
		int leftPlayers = players.CountList();
		//Add 1 to avoid picking 0, but allow picking itself anyway
		int ran = (rand() % leftPlayers) + 1;

		cout << "Result:" << endl;
		cout << "\t" << players.head->name << " has drawn " << ran << endl; //head always draws

		while (ran > 0) {
			players.SetHead(players.head->next); //Move the head down the list every count
			ran--;
		}

		cout << "\t" << players.head->name << " was eliminated." << endl;	
		//Delete the head
		players.DeleteNode(players.head);
		//Delete function handles the holes so the tail->next will be set as the new head
		players.SetHead(players.tail->next);
		
		cout << endl;
		round++; //increase round;
		system("pause");
		system("cls");
	}

	//Last one should be the head/tail, either one is fine
	cout << "=================" << endl;
	cout << "FINAL RESULT" << endl;
	cout << "=================" << endl;
	cout << "\t" << "The RNG Gods have decided for " << players.tail->name << ".\n"
		<< "\t" << players.head->name << " will go seek for reinforcements." << endl;
	cout << endl;
	system("pause");
}


int main()
{
	srand(time(NULL));

	//Create a linked list
	List* nightWatch = new List;

	cout << "Lord Commander!\n"
		<< "The White Walkers are here!\n"
		<< "We need to send someone to request for reinforcements from the other Kingdoms." << endl;

	cout << endl;

	cout << "[You decided to play a game and let the Gods of Ice and Water decide their fate]\n"
		<< "\n"
		<< "How many soldiers will you pick for the game? ";

	//Number of soldiers that will be used
	int numberOfPeeps;
	cin >> numberOfPeeps;

	if (numberOfPeeps == 0) {
		cout << endl;
		cout << "You decided to send yourself." << endl;
		system("pause");

		//Delete the linked list
		delete nightWatch;

		//End Game
		return 0;
	}
	else {
		cout << "Who are they? " << "(write " << numberOfPeeps << " names)" << endl;
	}

	//Type their names and create a new node
	while (numberOfPeeps > 0) {
		string name;
		cout << " -> ";
		cin >> name;

		nightWatch->AddNode(new Node{ name });
		numberOfPeeps--;
	}

	//Set a randomly new head 
	nightWatch->RandomlySetNewHead();

	system("cls");

	//Game continue until one is left
	PlayGame(*nightWatch);

	delete nightWatch;

	return 0;
}
#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include <windows.h>
#include <math.h>

using namespace std;

//Quiz_1 Suggest a Package

//Group some variables together
//Items to show and sell
struct StoreItems {
	string name[9] = { "Writings of Unfullfiled Dreams", "-101% Experience Modifier", "Random Permanent Debuff", "Nothing of Value",  "Goal Without Direction", "Direction without Motivation", "Motivation without Skills", "Skills without Goal", "Life" };
	int price[9] = { 60, 100, 220, 550, 1000, 5000, 10000, 30000, 500000 };
	int size = 9;
	int selected = 0; //Item that was selected
};

//Packages to suggest
struct StorePackages {
	string name[7] = { "Package #1", "Package #2", "Package #3", "Package #4", "Package #5", "Package #6", "Package #7" };
	int amount[7] = { 30750, 1780, 500, 250000, 13333, 150, 4050 }; //Start unsorted
	int size = 7;
};

//State of the store
int GREEN_COLOR = 10;
int PURPLE_COLOR = 13;
int YELLOW_COLOR = 14;
int DEFAULT_COLOR = 15;
bool storeIsOpen = true;

//Player related variables
int gold = 250;
int input;

//Initializer
void ValidatePurchase(int price, StoreItems storeItems, StorePackages storePackages);

//Add some color
void Color(int color) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbInfo;
	GetConsoleScreenBufferInfo(hConsole, &csbInfo);
	SetConsoleTextAttribute(hConsole, color);
	return;
}
void ResetColor(int previous = 15) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbInfo;
	GetConsoleScreenBufferInfo(hConsole, &csbInfo);
	SetConsoleTextAttribute(hConsole, previous);
	return;
}

//Standard Choice Checkers (y/n)
bool UserPressedY() {
	int choice = -1;
	while ((choice != 'y') && (choice != 'n')) {
		choice = _getch();
	}
	if (choice == 'y') {
		return true;
	}
	return false;
}

//Sort an int array in an ascending order
void SortIntArrayAsc(int array[], int arraySize) {
	for (int i = 0; i < arraySize; i++) {

		int check = array[i];
		int swapIndex = i;

		for (int ii = i; ii < arraySize; ii++) {
			//Find number greater than check then store that
			if (array[ii] < check) {
				check = array[ii];
				swapIndex = ii;
			}
		}

		//If check is not equal to array[i] start swapping
		if (array[i] != check) {
			array[swapIndex] = array[i];
			array[i] = check;
		}
	}
	return;
}

//Display Store Items
void DisplayStoreItems(StoreItems storeItems) {
	for (int i = 0; i < storeItems.size; i++) {
		Color(GREEN_COLOR);
		cout << storeItems.name[i];
		ResetColor(DEFAULT_COLOR);

		cout << ": ";

		Color(YELLOW_COLOR); 
		cout << storeItems.price[i];
		ResetColor(DEFAULT_COLOR);

		cout << endl;
	}
	return;
}

//Suggest a package based on price, players current gold and the just enough range
void SuggestPackage(StoreItems storeItems, StorePackages storePackages) {
	//Get amount of gold needed to buy the selected item
	float amountNeeded = storeItems.price[storeItems.selected] - gold;

	for (int i = 0; i < storePackages.size; i++) {

		//If amountNeeded is less than or equal to the amount you get in the package but not more than or equal to the range
		if ((amountNeeded <= storePackages.amount[i])) {
			cout << "Will you buy ";

			Color(PURPLE_COLOR);
			cout << storePackages.name[i];
			ResetColor();

			cout << " to get ";

			Color(YELLOW_COLOR);
			cout << storePackages.amount[i] << " Gold" << endl;
			ResetColor(); 

			cout << "and have a total amount of ";

			Color(YELLOW_COLOR);
			cout << storePackages.amount[i] + gold << " Gold";
			ResetColor();

			cout << "? (Y/N)" << endl;

			if (UserPressedY()) {
				cout << "------------------------------" << endl;

				cout << "Good Deal!" << endl;

				//Add Gold
				gold += storePackages.amount[i];
				cout << "You now have ";

				Color(YELLOW_COLOR);
				cout << gold << " Gold" << endl;
				ResetColor();

				 					

				//Validate the purchase Again	
				return ValidatePurchase(storeItems.price[storeItems.selected], storeItems, storePackages); 
			}

			//Exit Store
			storeIsOpen = false; 
			return;
		}
	}
	cout << "No Package available that is enough to buy " << storeItems.name[storeItems.selected] << endl;
	cout << "Please choose a different item." << endl;
	system("pause");
	system("cls");
	return;
}

//Check if item exists or purchase is valid
void ValidatePurchase(int price, StoreItems storeItems, StorePackages storePackages) {
	for (int i = 0; i < storeItems.size; i++) {

		//Check if the price matches one of the store Items
		if (price == storeItems.price[i]) {

			//Suggest a package if current gold is not enough
			if (price > gold) {
				cout << "------------------------------" << endl;
				cout << "You don't have enough ";

				Color(YELLOW_COLOR);
				cout << "Gold";
				ResetColor();

				cout << " to buy ";

				Color(GREEN_COLOR);
				cout << storeItems.name[i] << endl;
				ResetColor();

				storeItems.selected = i;
				SuggestPackage(storeItems, storePackages);
				return;
			}

			cout << "------------------------------" << endl;
			gold -= price;

			cout << "You bought ";

			Color(GREEN_COLOR);
			cout << storeItems.name[i];
			ResetColor();

			cout << " for ";

			Color(YELLOW_COLOR);
			cout << storeItems.price[i] << " Gold";
			ResetColor();

			cout << "!" << endl;
			Sleep(500);

			cout << "Thank you for your purchase." << endl;
			Sleep(500);

			cout << "Anything else? (Y/N)" << endl;
			if (UserPressedY()) {
				system("cls");
				return;
			}
			storeIsOpen = false; //Close the Store
			return;
		}
	}
	cout << "That item does not exist." << endl;
	cin.clear(); //Just incase if player inputs a char
	cin.ignore(INT_MAX, '\n');
	Sleep(500);
	system("cls");
	return;
}

int main()
{
	srand(time(NULL));
	int dialogue = rand() % 4; //0-3
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), DEFAULT_COLOR); //Set default color of text
	StoreItems storeItems;
	StorePackages storePackages;

	SortIntArrayAsc(storePackages.amount, storePackages.size); //Sort immediately

	while (storeIsOpen) {
		switch (dialogue) {
		case 0:
			cout << "Welcome to the Corner Store of Life!\nI have wares if you got coin." << endl;
			break;
		case 1:
			cout << "Welcome to the Corner Store of Life!\nAre you selling or buying? Doesn't matter you'll end up either way."<< endl;
			break;
		case 2:
			cout << "Welcome to the Corner Store of Life!\nIf you can't afford it, you can always get some more." << endl;
			break;
		case 3:
			cout << "Welcome to the Corner Store of Life!\nBrowse however long you want, you might end up here someday." << endl;
			break;
		}
		cout << endl;

		DisplayStoreItems(storeItems);
		cout << endl;

		cout << "Enter the Price of the item to buy it." << " (You have ";

		Color(YELLOW_COLOR);
		cout << gold << " Gold";
		ResetColor();

		cout << " remaining)" << endl;

		cin >> input;

		ValidatePurchase(input, storeItems, storePackages);
	}
	cout << "------------------------------" << endl;
	cout << "Come again!" << endl;
	system("pause");
	cout << endl;
	return 0;
}